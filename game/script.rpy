﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define e = Character("Eileen")


# The game starts here.

label start:

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene bg room

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "eileen happy.png" to the images
    # directory.

    show eileen sad

    # These display lines of dialogue.
    e "..."

    e "...Again?"

    e "Why?"

    e "What do you have to prove?"

    "She looks at you. A lump forms in your throat as you try to explain."

    "She shakes her head."

    e "Stop."

    e "I don't want any of your excuses."

    e "...Just...Just don't take too long this time, okay?..."

    "You nod. You desperately wish for the ability to keep that promise."

    "Her lips quirked up just a bit, and it was the last thing you saw before the world is engulfed in fog again."

    # This ends the game.

    return
