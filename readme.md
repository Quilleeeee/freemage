Hello, and welcome to Freemage.

Freemage is an open source visual novel where anyone can contribute to the story's common canon universe.

The canon is maintained, and updated by the community. If you have any ideas on an undeveloped part of the canon, or an alternative to an existing one, feel free to share.

You can reach me by the following email address: monikabestgirl@waifu.club

Yes, the email actually works.

Or, maybe once I know for sure that there are people who want to contribute, I may start up a discord server where we can discuss lore and world-building.
